﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class UILineRenderer : MonoBehaviour
{

    [SerializeField] AnimationCurve width;
    [SerializeField] AnimationCurve curveX;
    [SerializeField] AnimationCurve curveY;
    [SerializeField] float tesselation = 10;
    [SerializeField] float offsetSpeed = 0.1f;
    [SerializeField] float animOffset = 0f;
    [SerializeField] float widthAnimCoef = 1f;
    private UIVertex[] _quad1 = new UIVertex[4];
    private UIVertex[] _quad2 = new UIVertex[4];
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private float offset = 0f;
    private Mesh mesh;

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
    }

    private void Start()
    {
        meshFilter.sharedMesh = new Mesh();
        mesh = meshFilter.sharedMesh;

        float offset = animOffset;
        foreach (Transform t in transform)
        {
            var animKey = t.GetComponent<NoiseMover>();
            if (animKey != null)
            {
                animKey.offset = offset;
                offset += 1f;
            }
        }
    }

    void Update()
    {
        OnPopulateMesh();
    }

    protected Mesh OnPopulateMesh()
    {

        if (!gameObject.activeInHierarchy)
        {
            return null;
        }
        


        if (meshRenderer.material != null)
        {
            offset += Time.deltaTime * (Mathf.PerlinNoise(Time.time, 0f) + 1f) * offsetSpeed;
            meshRenderer.material.SetTextureOffset("_MainTex", Vector2.right * (offset % 1f));
        }

        mesh.Clear();

        List<Vector3> uvs = new List<Vector3>();
        List<Vector3> positions = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> tris = new List<int>();

        Vector2 pos1 = Vector2.zero;
        Vector2 pos2 = Vector2.zero;
        Vector3 normal = Vector3.up;

        int posCount = transform.childCount;
        
        float totalLen = 0f;

        bool isFirst = true;
        curveX = new AnimationCurve();
        curveY = new AnimationCurve();
        var newWidth = new AnimationCurve();
        foreach (var key in width.keys)
        {
            newWidth.AddKey(key.time, key.value * (1f + widthAnimCoef * (0.8f - Mathf.PerlinNoise((Time.time + animOffset) * 0.1f, key.time))));
        }

        foreach (RectTransform t in transform)
        {
            curveX.AddKey(totalLen, t.anchoredPosition.x);
            curveY.AddKey(totalLen, t.anchoredPosition.y);
            if (isFirst)
            {
                isFirst = false;
                pos2 = t.anchoredPosition;
                continue;
            }
            pos1 = pos2;
            pos2 = t.anchoredPosition;
            totalLen += Vector2.Distance(pos1, pos2);
        }

        int triCounter = 0;

        Vector3 p1, p2, p3, p0;

        p1 = new Vector3(curveX.Evaluate(0f), curveY.Evaluate(0f), 0f) - normal * newWidth.Evaluate(0f);
        p2 = new Vector3(curveX.Evaluate(0f), curveY.Evaluate(0f), 0f) + normal * newWidth.Evaluate(0f);

        for (float i = 0; i < tesselation - float.Epsilon; i += 1f)
        {
            float curvesPos1 = i / tesselation;
            float curvesPos2 = (i + 1f) / tesselation;

            p0 = p1;
            p3 = p2;
            p1 = new Vector3(curveX.Evaluate(curvesPos2 * totalLen), curveY.Evaluate(curvesPos2 * totalLen), i * 100f  / tesselation) - normal * newWidth.Evaluate(curvesPos2);
            p2 = new Vector3(curveX.Evaluate(curvesPos2 * totalLen), curveY.Evaluate(curvesPos2 * totalLen), i * 100f / tesselation) + normal * newWidth.Evaluate(curvesPos2);

            float ax = p2.x - p0.x;
            float ay = p2.y - p0.y;
            float bx = p3.x - p1.x;
            float by = p3.y - p1.y;

            float cross = ax * by - ay * bx;

            if (cross != 0)
            {
                float cy = p0.y - p1.y;
                float cx = p0.x - p1.x;

                float s = (ax * cy - ay * cx) / cross;
                if (s > 0 && s < 1)
                {
                    float t = (bx * cy - by * cx) / cross;
                    if (t > 0 && t < 1)
                    {

                        float q0 = 1 / (1 - t);
                        float q1 = 1 / (1 - s);
                        float q2 = 1 / t;
                        float q3 = 1 / s;
                        float prevDir = curveX.Evaluate(curvesPos1 * totalLen) - curveX.Evaluate((curvesPos1 - 1f / tesselation) * totalLen);
                        float curDir = curveX.Evaluate(curvesPos2 * totalLen) - curveX.Evaluate(curvesPos1 * totalLen);
                        float nextDir = curveX.Evaluate((curvesPos2 + 1f / tesselation) * totalLen) - curveX.Evaluate(curvesPos2 * totalLen);
                        Color color1 = Color.white;
                        Color color2 = Color.white;
                        if (Mathf.Sign(prevDir) != Mathf.Sign(curDir))
                        {
                            color1.a = 0f;
                        }
                        if (Mathf.Sign(nextDir) != Mathf.Sign(curDir))
                        {
                            color2.a = 0f;
                        }

                        uvs.Add(new Vector3(curvesPos1 * q0, 0, q0));
                        uvs.Add(new Vector3(curvesPos1 * q3, q3, q3));
                        uvs.Add(new Vector3(curvesPos2 * q2, q2, q2));
                        uvs.Add(new Vector3(curvesPos2 * q1, 0, q1));
                        colors.Add(color1);
                        colors.Add(color1);
                        colors.Add(color2);
                        colors.Add(color2);
                        positions.Add(p0);
                        positions.Add(p3);
                        positions.Add(p2);
                        positions.Add(p1);
                        tris.Add(triCounter);
                        tris.Add(triCounter + 1);
                        tris.Add(triCounter + 2);
                        tris.Add(triCounter);
                        tris.Add(triCounter + 2);
                        tris.Add(triCounter + 3);
                        triCounter += 4;
                    }
                }
            }
                
            


            
        }

        mesh.SetVertices(positions);
        mesh.SetUVs(0, uvs);
        mesh.SetTriangles(tris, 0);
        mesh.SetColors(colors);
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();
        positions.Clear();
        uvs.Clear();
        tris.Clear();
        return mesh;
    }
}
