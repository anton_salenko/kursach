#if UNITY_IOS || UNITY_IPHONE
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class PostBuildProcessor : MonoBehaviour
{
	[PostProcessBuild]
	public static void ChangeXcodePlist( BuildTarget buildTarget, string pathToBuiltProject )
	{

		if( buildTarget == BuildTarget.iOS )
		{

			// Get plist
			string plistPath = pathToBuiltProject + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString( File.ReadAllText( plistPath ) );

			// Get root
			PlistElementDict rootDict = plist.root;
			rootDict.SetString( "NSAppleMusicUsageDescription", "Needed for saving pictures, taken by the user." );
			rootDict.SetString( "NSCameraUsageDescription", "Needed to activate and play Augmented Reality content" );
			rootDict.SetString( "NSMicrophoneUsageDescription", "Needed for saving pictures, taken by the user." );
			rootDict.SetString( "NSMotionUsageDescription", "Needed to activate and play Augmented Reality content" );
			rootDict.SetString( "NSPhotoLibraryUsageDescription", "Needed for saving pictures, taken by the user." );
			rootDict.SetString( "NSPhotoLibraryAddUsageDescription","Needed for saving pictures, taken by the user." );
            rootDict.SetString( "NSLocationWhenInUseUsageDescription", "Needed to activate and play Augmented Reality content" );
			// Write to file
			File.WriteAllText( plistPath, plist.WriteToString() );

#if !UNITY_CLOUD_BUILD
			Debug.Log ("OnPostprocessBuild");
			ProcessPostBuild (buildTarget, pathToBuiltProject);
#endif
		}
	}

    private static void ProcessPostBuild (BuildTarget buildTarget, string path)
	{
		string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

		PBXProject proj = new PBXProject();
		proj.ReadFromString(File.ReadAllText(projPath));

		string target = proj.TargetGuidByName("Unity-iPhone");

		//Required Frameworks
		proj.AddFrameworkToProject(target, "ARKit.framework", false);

        //Optional Frameworks
        proj.AddFrameworkToProject(target, "UserNotifications.framework", true);
        proj.AddFrameworkToProject(target, "Security.framework", true);

		//
		string projectString = proj.WriteToString();
		File.WriteAllText(projPath, projectString);
    }	
}
#endif

