﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarkBehaviour : MonoBehaviour {

    [SerializeField] Text markText;
    [SerializeField] string format;

    public static MarkBehaviour Instance = null;

    // Use this for initialization
    void Start () {
        Instance = this;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMark(float mark)
    {
        markText.text = string.Format(format, mark);
    }
}
