﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

[RequireComponent(typeof(Light))]
public class FireLight : MonoBehaviour {

    Light light;
    [SerializeField] float min;
    [SerializeField] float max;
    [SerializeField] float shake;
    Vector3 position = Vector3.zero;
	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
        position = transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        light.intensity = Mathf.PerlinNoise(Time.time * 2 + 10, Time.time * 1) * (max - min) + min;
        transform.localPosition = position + new Vector3(Mathf.PerlinNoise(Time.time * 2.2f + 16, Time.time * 1), Mathf.PerlinNoise(Time.time * 1.5f + 10, Time.time * 2.6f), Mathf.PerlinNoise(Time.time * 2.1f + 11, Time.time * 1.11f)) * shake;
	}
}
