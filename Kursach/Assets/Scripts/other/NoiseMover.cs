﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(RectTransform))]
public class NoiseMover : MonoBehaviour {

    public float offset = 0f;
    [SerializeField] float speed = 1f;
    [SerializeField] Vector2 bounds = Vector2.one * 100f;
    
    private Vector2 startPos;
    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        startPos = rectTransform.anchoredPosition - bounds / 2f;
    }

    private void Update()
    {
        rectTransform.anchoredPosition = startPos + bounds * (Mathf.PerlinNoise((Time.time + offset) * speed, 0f) + Mathf.PerlinNoise((Time.time + offset) * speed * 5f, 0.5f) * 0.2f);
    }
}
