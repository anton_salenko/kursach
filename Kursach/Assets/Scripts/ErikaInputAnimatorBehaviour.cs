﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErikaInputAnimatorBehaviour : MonoBehaviour {

    [SerializeField] string speedValueName;
    [SerializeField] string rotationValueName;
    [SerializeField] Animator animator;

    [SerializeField] ArrowBehaviour arrow;

    [SerializeField] Transform leftHandTransform;
    [SerializeField] Transform rightHandTransform;
    [SerializeField] Transform arrowHolder;
    [SerializeField] float arrowPosition;
    [SerializeField] Transform cameraTransform;
    [SerializeField] CameraCharacterFolowingBehaviour cameraController;
    [SerializeField] AudioSource shotSound;
     
    [SerializeField] float blendValuesAcceleration = 2f;

    private float speed = 0f;
    private float rotation = 0f;
    bool dodge = false;
    bool aiming = false;

    private ArrowBehaviour curArrow;

      
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        dodge = false;
        CheckAiming();
        SetArrowPosition();

        if (Input.GetKey(KeyCode.W))
        {
            speed = Mathf.Clamp(speed + blendValuesAcceleration * Time.deltaTime, -1, 1);
            if (Input.GetKeyDown(KeyCode.Space) && !dodge)
            {
                animator.SetTrigger("Dodge fwd");
                dodge = true;
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            speed = Mathf.Clamp(speed - blendValuesAcceleration * Time.deltaTime, -1, 1);
            if (Input.GetKeyDown(KeyCode.Space) && !dodge)
            {
                animator.SetTrigger("Dodge bwd");
                dodge = true;

            }
        }
        else
        {
            if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
            {
                speed = Mathf.Clamp01(Mathf.Abs(speed) - blendValuesAcceleration * Time.deltaTime) * Mathf.Sign(speed);
            }
        }

        if (Input.GetKey(KeyCode.A))
        {
            rotation = Mathf.Clamp(rotation - blendValuesAcceleration * Time.deltaTime, -1, 1);
            speed = Mathf.Clamp(speed + blendValuesAcceleration * Time.deltaTime, -1, 1);
            if (Input.GetKeyDown(KeyCode.Space) && !dodge)
            {
                animator.SetTrigger("Dodge left");
                dodge = true;

            }
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rotation = Mathf.Clamp(rotation + blendValuesAcceleration * Time.deltaTime, -1, 1);
            speed = Mathf.Clamp(speed + blendValuesAcceleration * Time.deltaTime, -1, 1);
            if (Input.GetKeyDown(KeyCode.Space) && !dodge)
            {
                animator.SetTrigger("Dodge right");
                dodge = true;

            }
        }
        else
        {
            rotation = Mathf.Clamp01(Mathf.Abs(rotation) - blendValuesAcceleration * Time.deltaTime) * Mathf.Sign(rotation);
        }
        
        animator.SetBool("Aiming", aiming);

        if (Input.GetKey(KeyCode.LeftShift))
        {

        }
        else if (Mathf.Abs(speed) > 0.5f)
        {
            speed -= Mathf.Sign(speed) * blendValuesAcceleration * Time.deltaTime * 2f;
            if (Mathf.Abs(speed) < 0.5f)
            {
                speed = 0.5f * Mathf.Sign(speed);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Jump");
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            animator.SetTrigger("Shot");
            MakeShot();
        }
        

        animator.SetFloat(speedValueName, speed);
        animator.SetFloat(rotationValueName, rotation);

    }

    private void CheckAiming()
    {
        bool wasAiming = aiming;
        aiming = Input.GetKey(KeyCode.Mouse1);
        if (wasAiming && !aiming)
        {
            if (curArrow != null)
            {
                Destroy(curArrow.gameObject);
                curArrow = null;
            }
            cameraController.UnAim();
        }

        if (!wasAiming && aiming)
        {
            cameraController.Aim();
        }
    }

    private void SetArrowPosition()
    {
        if (curArrow == null)
        {
            return;
        }


        curArrow.transform.forward = (leftHandTransform.position - rightHandTransform.position).normalized;
        curArrow.transform.position = rightHandTransform.position + curArrow.transform.forward * arrowPosition;

    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log("Hit");
            
    }

    public void SetForward(Vector3 fwd)
    {
        float rotationDifference = Mathf.Acos(Vector3.Dot(fwd, transform.forward)) * Mathf.Rad2Deg; 

        if (aiming)
        {
            transform.forward = fwd;
            return;
        }

        if (Mathf.Abs(speed) < 0.2f && rotationDifference > 40f) 
        {
            rotation = Mathf.Sign(Vector3.Dot(transform.right, fwd)); 
        }
        else if (Mathf.Abs(speed) >= 0.2f)
        {
            transform.forward = fwd;
        }
    }

    private void MakeShot()
    {
        if (curArrow == null)
        {
            return;
        }
        var releasedArrow = curArrow;
        curArrow = null;
        releasedArrow.transform.forward = cameraTransform.transform.forward;
        releasedArrow.Shot(1000);
        shotSound.Play();
    }
    
    public void InstantianeNewArrow()
    {
        if (curArrow != null)
        {
            Destroy(curArrow.gameObject);
        }
        curArrow = Instantiate(arrow, arrowHolder);
        curArrow.gameObject.SetActive(true);
    }
}
