﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayLightBehaviour : MonoBehaviour {

    [SerializeField] new Light light;
    [SerializeField] UnityEngine.Experimental.Rendering.HDPipeline.BakingSky skySettings;
    [SerializeField] 

    // Use this for initialization
    void Start () {
		
	}

    float value = 0f;

	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.KeypadPlus))
        {
            value += Time.deltaTime * 0.5f;
        }

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            value -= Time.deltaTime * 0.5f;
        }

        light.intensity = (Mathf.Cos(value) + 1f) / 2f;
    }
}
