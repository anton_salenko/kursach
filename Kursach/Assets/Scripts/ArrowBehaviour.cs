﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(AudioSource))]
public class ArrowBehaviour : MonoBehaviour {

    new Rigidbody rigidbody;
    new Collider collider;
    AudioSource hitSound;

    [SerializeField] TrailRenderer trailRenderer;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        hitSound = GetComponent<AudioSource>();
        collider.enabled = false;
        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Shot(float power)
    {
        collider.enabled = true;
        rigidbody.useGravity = true;
        rigidbody.isKinematic = false;
        rigidbody.AddForce(transform.forward * power);
        trailRenderer.emitting = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            return;
        }
        rigidbody.velocity = Vector3.zero;
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
        collider.enabled = false;
        trailRenderer.emitting = false;
        hitSound.Play();
        Destroy(trailRenderer.gameObject, 20f);

        if (collision.collider.tag == "Target")
        {
            var point = collision.contacts[0].point;
            var center = collision.collider.GetComponent<TargetBehaviour>().center.position;
            float dist = Vector3.Distance(center, point);
            float mark = Mathf.Clamp(10f - (dist / 0.534f) * 10f, 0f, 10f);
            if (MarkBehaviour.Instance != null)
            {
                MarkBehaviour.Instance.SetMark(mark);

            }
        }
        
    }

    private void FixedUpdate()
    {
        if (rigidbody.isKinematic)
        {
            return;
        }

        transform.forward = rigidbody.velocity.normalized;
    }

}
