﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCharacterFolowingBehaviour : MonoBehaviour {

    [SerializeField] Transform characterTransform;
    [SerializeField] Vector3 offset;
    [SerializeField] float maxDistance;
    [SerializeField] float minDistance;
    [SerializeField] float defaultDistance;
    [SerializeField] float scrollSpeed = 1f;
    [SerializeField] float rotationspeed = 1f;

    [SerializeField] ErikaInputAnimatorBehaviour erikaInputAnimatorBehaviour;

    [Header("Aim")]
    [SerializeField] Transform aimTransform;
    [SerializeField] Transform aimCameraTransform;
    [SerializeField] float aimingTime = 1f;
    [SerializeField] Transform characterSpineTransform;
    [SerializeField] GameObject aimImage;

    private Quaternion aimRotation;
    private float distance = 0;
    private Vector2 prevFrameMousePosition;
    private bool aiming = false;
    private Vector3 aimingPosition;
    Coroutine aimingCoroutine = null;

	// Use this for initialization
	void Start () {
        transform.position = characterTransform.position + offset.normalized * defaultDistance;
        //Cursor.lockState = CursorLockMode.Confined; // todo
        distance = defaultDistance;

        prevFrameMousePosition = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

    }
	
	// Update is called once per frame
	void Update () {

        
        distance = Mathf.Clamp(distance + Input.mouseScrollDelta.y * scrollSpeed, minDistance, maxDistance);

        Vector2 mousePositionDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        float xRotation = Mathf.Acos(Vector3.Dot(offset.normalized, Vector3.up)) * Mathf.Rad2Deg;
        xRotation = Mathf.Clamp(xRotation + mousePositionDelta.y * rotationspeed, 10, 170);
        xRotation *= Mathf.Deg2Rad;

        Vector2 offset2D = new Vector2(offset.x, offset.z).normalized;

        prevFrameMousePosition = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        float yRotation = Mathf.Acos(Vector2.Dot(offset2D, Vector2.right)) * Mathf.Rad2Deg * Mathf.Sign(offset2D.y);

        yRotation = yRotation - mousePositionDelta.x * rotationspeed;
        yRotation *= Mathf.Deg2Rad;

        offset = new Vector3(Mathf.Sin(xRotation) * Mathf.Cos(yRotation), Mathf.Cos(xRotation), Mathf.Sin(xRotation) * Mathf.Sin(yRotation));

        if (aiming)
        {
            transform.position = aimingPosition;
            transform.rotation = aimRotation;
            
        }
        else
        {
            transform.position = characterTransform.position + Vector3.up * 1.5f + offset.normalized * distance;
            transform.LookAt(characterTransform.position + Vector3.up * 1.5f);
        }


        erikaInputAnimatorBehaviour.SetForward(new Vector3(-offset.x, 0f, -offset.z).normalized);
    }

    float yAimRotation = 0f;
    private void LateUpdate()
    {
        if (aiming)
        {
            yAimRotation -= Input.GetAxis("Mouse Y") * rotationspeed;
            yAimRotation = Mathf.Clamp(yAimRotation, -75f, 75f);
            characterSpineTransform.eulerAngles = new Vector3(characterSpineTransform.eulerAngles.x, characterSpineTransform.eulerAngles.y, yAimRotation);
        }
    }

    public void Aim()
    {
        if (aimingCoroutine != null)
        {
            StopCoroutine(aimingCoroutine);
            aimingCoroutine = null;
        }
        aimingCoroutine = StartCoroutine(Aiming());
    }

    public void UnAim()
    {
        if (aimingCoroutine != null)
        {
            StopCoroutine(aimingCoroutine);
            aimingCoroutine = null;
        }
        aimingCoroutine = StartCoroutine(UnAiming());
    }

    private IEnumerator Aiming( )
    {
        float time = 0f;
        Vector3 cameraPos = transform.position;
        yAimRotation = 0f;
        aiming = true;
        var startRotation = Quaternion.LookRotation(characterTransform.position + Vector3.up * 1.5f - cameraPos, Vector3.up);
        while (time < aimingTime)
        {
            time += Time.deltaTime;
            aimingPosition = Vector3.Lerp(cameraPos, aimCameraTransform.position, time / aimingTime);
            aimRotation = Quaternion.Slerp(startRotation, Quaternion.LookRotation(aimTransform.position - aimCameraTransform.position, Vector3.up), time / aimingTime);

            yield return null;
        }
        aimImage.SetActive(true);
        while (aiming)
        {
            aimingPosition = aimCameraTransform.position;
            aimRotation = Quaternion.LookRotation(aimTransform.position - aimCameraTransform.position, Vector3.up);
            yield return null;
        }

    }

    private IEnumerator UnAiming()
    {
        float time = 0f;
        Vector3 cameraPos = transform.position;
        aimImage.SetActive(false);
        var startRotation = Quaternion.LookRotation(aimTransform.position - aimCameraTransform.position, Vector3.up);
        while (time < aimingTime)
        {
            time += Time.deltaTime;
            Vector3 target = characterTransform.position + Vector3.up * 1.5f + offset.normalized * distance;
            aimingPosition = Vector3.Lerp(aimCameraTransform.position, target, time / aimingTime);
            aimRotation = Quaternion.Slerp(startRotation, Quaternion.LookRotation(characterTransform.position + Vector3.up * 1.5f - target, Vector3.up),  time / aimingTime);


            yield return null;
        }

        aiming = false;
    }
}
